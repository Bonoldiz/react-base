import { LOGIN_ERROR,LOGIN_LOADING,LOGIN_SUCCESS } from '../types';
import { alert } from "./alertActions";
import authUtils from '../utils/authUtils';

import {isLoaded,isLoading} from "../actions/appActions";

export const login = (email,password) => {
    return async dispatch => {
        dispatch({type:LOGIN_LOADING,payload:null});

        var response = await authUtils.Auth.login(email,password);
        
        if(response.success){
            dispatch({type:LOGIN_SUCCESS,payload:response});
            dispatch(alert("success","Benvenuto :D"));
        }else{
            dispatch({type:LOGIN_ERROR,payload:response});
            dispatch(alert("warning",response.message));
        }

    }
}