import {APP_LOADED,APP_LOADING} from "../types";

export const isLoading = (dispatch) => {
    dispatch({type:APP_LOADING,payload:null})
}

export const isLoaded = (dispatch) => {
    dispatch({type:APP_LOADED,payload:null})
}