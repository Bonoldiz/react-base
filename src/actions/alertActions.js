import {
    SET_ALERT_MESSAGE
  } from '../types';

export const alert = (type,message,seconds = 2.5) => {
    return dispatch => {
        dispatch({type:SET_ALERT_MESSAGE,payload:{"message":message,"type":type}});
        setTimeout(()=>{
            dispatch({type:SET_ALERT_MESSAGE,payload:{"message":null,"type":type}});
        },(seconds*1000));
    }
}