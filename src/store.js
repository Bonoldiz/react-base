import { createStore, applyMiddleware , compose} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';

//import Middlewares
import { localStorageMiddleware } from  './middlewares';

//activating dev mode
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    
export default function configureStore() {
    return createStore(
        rootReducer,
        composeEnhancer(applyMiddleware(thunk,localStorageMiddleware)),
    );
}