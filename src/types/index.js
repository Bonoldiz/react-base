// APP ACTIONS
export const APP_LOADED = 'APP_LOADED';
export const APP_LOADING = 'APP_LOADING';

export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const SHOW_ALERT = 'SHOW_ALERT';
export const HIDE_ALERT = 'HIDE_ALERT';

// AUTH ACTIONS
export const LOGIN = "LOGIN";
export const LOGIN_LOADING = "LOGIN_LOADING";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";

export const LOGOUT = "LOGOUT";

export const UPDATE_FIELD_AUTH = "UPDATE_FIELD_AUTH";
