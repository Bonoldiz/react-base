import React, { Component } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = state => ({ ...state.alertReducer });

class Alert extends Component {
    constructor() {
        super();
    }

    getClasses = (type) => {
        return ("alert alert-"+type+" system-alert ");
    }

    render() {
        const message = this.props.message;
        const type = this.props.type;
        
        return (
            <div>
                {message != null &&
                    <div className={this.getClasses(type)} id="systemAlert">
                        <strong>{type.toUpperCase()}</strong> | {message}
                    </div>
                }
                
            </div>
        );
    }
  }

export default connect (mapStateToProps, null) (Alert);