import { Switch, Route , Redirect} from 'react-router-dom';

import React, { Component } from 'react';



const ProtectedRoute = ({ component: Comp, loggedIn, path, ...rest }) => {
    console.log(loggedIn);
    return (
        <Route path={path} {...rest} render={(props) => {return loggedIn ? ( <Comp {...props} />) : (<Redirect to={{pathname: "/login",}}/>);}}/>);
};

export default ProtectedRoute;