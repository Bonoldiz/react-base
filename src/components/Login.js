import React, { Component } from 'react';
import { connect } from 'react-redux';
import { UPDATE_FIELD_AUTH } from '../types';
import { login } from "../actions/authActions";
import { isLoading, isLoaded } from '../actions/appActions';

const mapStateToProps = state => ({ ...state.authReducer });

const mapDispatchToProps = dispatch => ({
    onChangeEmail: value =>{
        dispatch({ type: UPDATE_FIELD_AUTH, key: 'email', value })},
    onChangePassword: value =>
        dispatch({ type: UPDATE_FIELD_AUTH, key: 'password', value }),
    onSubmit:(email, password) => {
        dispatch(login(email,password));
    }

});

class Login extends Component {
    constructor() {
        super();
        // When submmiting form
        this.submitForm = (email, password) => ev => {
            ev.preventDefault();
            this.props.onSubmit(email,password);
        };
        // On form changing
        this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
        this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    }


    render() {
        const email= this.props.email;
        const password = this.props.password;
        return (
            <div className="container py-5">
                <div className="row">
                    <div className="col-md-12">
                        <h2 className="text-center text-black mb-4">FottiBoska Login</h2>
                        <div className="row">
                            <div className="col-md-6 mx-auto">

                                <div className="card rounded-0">
                                    <div className="card-header">
                                        <h3 className="mb-0">Login</h3>
                                    </div>
                                    <div className="card-body">
                                        <form className="form" role="form" id="formLogin" onSubmit={this.submitForm(email,password)}>
                                            <div className="form-group">
                                                <label>Email</label>
                                                <input type="text" className="form-control form-control-lg rounded-0" name="email" id="email" required="" value={email} onChange={this.changeEmail}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Password</label>
                                                <input type="password" className="form-control form-control-lg rounded-0" id="password" required="" value={password} onChange={this.changePassword}/>                                            
                                            </div>
                                            <button type="submit" className="btn btn-success btn-lg float-right" id="btnLogin" >Login</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
  }
export default connect (mapStateToProps, mapDispatchToProps) (Login);