
import authUtils from './utils/authUtils';

import {
  LOGOUT,
  LOGIN_SUCCESS,
  LOGIN_ERROR
} from './types';


const localStorageMiddleware = store => next => action => {
    if (action.type === LOGIN_SUCCESS) {
      if (!action.error) {
        window.localStorage.setItem('token', action.payload.token);
        authUtils.setToken(action.payload.token);
      }
    } else if (action.type === LOGIN_ERROR) {
      authUtils.setToken(null);
    } else if (action.type === LOGOUT) {
      window.localStorage.setItem('token', '');
      authUtils.setToken(null);
    }
  
    next(action);
  };

  export { localStorageMiddleware }