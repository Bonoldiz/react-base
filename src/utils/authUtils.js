import axios from "axios";

//Config Axios

let token = null;


const setToken = (_token) => {
    token = _token;

    axios.interceptors.request.use(
        (config) => {
          if (token) {
            config.headers['Authorization'] = `Bearer ${ token }`;
          }
          return config;
        }, 
      
        (error) => {
          return Promise.reject(error);
        }
    );    
}


// URL backend
const URL = 'http://backend.fottiboska.it';


// Request const for requests
const request = {
    get: (url) => {
        return axios.get(`${URL}${url}`)
    },
    post: (url,body) =>{
        return axios.post(`${URL}${url}`, body).then((res)=>{return res.data}).catch((err)=>{return err.response.data});
    },
}

// Auth methods

const Auth = {
    current: (token) =>{
        
    },
    login: async (email, password) => {
        return await request.post('/api/login', {"email":email,"password":password});
    },
    register: (username, email, password) =>{

    },
    save: user =>{
      
    }
};

const isLogged = () => {
    if (token) {
        return true;
    }
    return false;

    
}



export default {
    Auth,
    setToken,
    isLogged
}