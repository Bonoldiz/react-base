import {
    UPDATE_FIELD_AUTH,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    LOGIN_LOADING
  } from '../types';
  
export default (state = {}, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                errors: action.success ? action.payload.success : null
            };
        case LOGIN_ERROR:
            return {
                ...state,
                message: action.payload.message
            };
        case UPDATE_FIELD_AUTH:
            return { ...state, [action.key]: action.value };
        case LOGIN_LOADING:
            return state;
        default:
            return state;
        }
    return state;
};