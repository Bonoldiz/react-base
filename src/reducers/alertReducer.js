import {
    HIDE_ALERT,
    SHOW_ALERT,
    SET_ALERT_MESSAGE
  } from '../types';
  
export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALERT_MESSAGE:
            return {
                ...state,
                message: action.payload.message,
                type: action.payload.type
            };
        case SHOW_ALERT:
            return {
                ...state,
                isVisible: true
            };
        case HIDE_ALERT:
            return {
                ...state,
                isVisible: false
            };
        default:
            return state;
        }
    return state;
};