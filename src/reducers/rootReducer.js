import { combineReducers } from 'redux';

import authReducer from './authReducer';
import alertReducer from './alertReducer';
import commonReducer from './commonReducer';

export default combineReducers({
    commonReducer,
    alertReducer,
    authReducer
});