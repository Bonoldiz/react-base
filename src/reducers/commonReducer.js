import {
    APP_LOADED,
    APP_LOADING
  } from '../types';
  
export default (state = {}, action) => {
    switch (action.type) {
        case APP_LOADED:
            return {
                ...state,
                isAppLoading:false
            };
        case APP_LOADING:
            return {
                ...state,
                isAppLoading: true
            };
        default:
            return state;
        }
    return state;
};