import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

// STRUCTURE

import Alert from "./structure/Alert";
import ProtectedRoute from "./structure/protectedRoute";
import Loader from './structure/Loader';

// COMPONENTS 
import Login from './components/Login';
import Dashboard from "./components/dashboard/Dashboard";

import authUtils from "./utils/authUtils";

import "./style.css";

// ACTIONS
import {isLoaded,isLoading} from "./actions/appActions";

const mapStateToProps = state => ({ ...state.commonReducer });

const mapDispatchToProps = dispatch => ({
  isLoading:()=>{isLoading(dispatch)},
  isLoaded:()=>{isLoaded(dispatch)},
});


class App extends Component {
  constructor() {
    super()
  }

  componentWillMount() {
    this.props.isLoading();
  }

  componentDidMount(){
    this.props.isLoaded();
  }

  render() {
    const isAppLoading = this.props.isAppLoading;

    return (
      <div>
        <Alert />

        { isAppLoading  && <Loader /> }

        <Switch>
          <Route path="/login" component={Login}/>
          <ProtectedRoute path="/" loggedIn={authUtils.isLogged()} component={Dashboard} />
        </Switch>
      </div>
    );
  }
}

export default connect (mapStateToProps, mapDispatchToProps)  (App);
